//Lee Vecchiarelli, 2035024
package LinearAlgebra;
public class Vector3dValueTest {
    public static void main(String[] args)
    {
        Vector3d vec1 = new Vector3d(10, 20, 30);
        Vector3d vec2 = new Vector3d(2, 3, 5);
        System.out.println(vec1.getX() + ", " + vec1.getY() + ", " + vec1.getZ());
        System.out.println(vec1.magnitude());
        System.out.println(vec1.dotProduct(vec2));
        Vector3d addedVector = vec1.add(vec2);
        System.out.println(addedVector.getX() + ", " + addedVector.getY() + ", " + addedVector.getZ());
    }
}
