//Lee Vecchiarelli, 2035024
public class Vector3d{
    final   private double X;
    final   private double Y;
    final   private double Z;

    public Vector3d(double x, double y, double z){
        this.X = x;
        this.Y = y;
        this.Z = z;
    }

    public double getX(){
        return this.X;
    }
    public double getY(){
        return this.Y;
    }
    public double getZ(){
        return this.Z;
    }

    public double magnitude(){
        double squared = Math.pow(this.X,2) + Math.pow(this.Y,2) + Math.pow(this.Z,2);
        return Math.sqrt(squared);
    }

    public double dotProduct(Vector3d vec){
        return (this.X*vec.getX()) + (this.Y*vec.getY()) + (this.Z*vec.getZ());
    }

    public Vector3d add(Vector3d vec){
        Vector3d newVector = new Vector3d(this.X+vec.getX(), this.Y+vec.getY(), this.Z+vec.getZ());
        return newVector;
    }
}
