//Lee Vecchiarelli, 2035024
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class Vector3dTests{
    @Test
    public void testMagnitude(){
        Vector3d v = new Vector3d(10, 20, 30);
        assertEquals(37.41,v.magnitude(), 0.01);
    }
    @Test
    public void testReturns(){
        Vector3d v = new Vector3d(45, 6, 8);
        assertEquals(45, v.getX());
        assertEquals(6, v.getY());
        assertEquals(8, v.getZ());
    }
    @Test
    public void testDotProduct(){
        Vector3d vec1 = new Vector3d(10, 20, 30);
        Vector3d vec2 = new Vector3d(2, 3, 5);
        assertEquals(230.0, vec1.dotProduct(vec2));
    }
    @Test
    public void testAdd(){
        Vector3d vec1 = new Vector3d(5, 12, 9);
        Vector3d vec2 = new Vector3d(2, 3, 5);
        Vector3d vec3 = vec1.add(vec2);
        assertEquals(7, vec3.getX());
        assertEquals(15, vec3.getY());
        assertEquals(14, vec3.getZ());
    }
}